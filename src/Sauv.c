/* DE CASTRO, COMBE
 * TD2
 * Sauv.c */


#include "../include/Sauv.h"

void ecritListe(FILE *fichier, UListe lst){
	UListe tmp;
	tmp = lst;

	while (tmp != NULL){
		fprintf(fichier, "%c %c %d %d |\n", tmp->couleur, tmp->genre, tmp->posX, tmp->posY);
		tmp = tmp->suiv;
	}
}

void ecritFichier(UListe lstR, UListe lstB, UListe lstP){
	FILE *fichier = NULL;

	if ((fichier = fopen("sauv.txt", "w+")) == NULL){
		fprintf(stderr, "Le fichier n'a pas pu s'ouvrir correctement\n");
		exit(EXIT_FAILURE);
	}

	ecritListe(fichier, lstR);
	ecritListe(fichier, lstB);
	ecritListe(fichier, lstP);
	fclose(fichier);
}


int recupSauv(Monde *monde){
	FILE *fichier = NULL;
	char couleur, genre;
	int posX, posY;
	UListe unite;

	if ((fichier = fopen("sauv.txt", "r")) == NULL){
		fprintf(stderr, "Le fichier n'a pas pu s'ouvrir correctement\n");
		return 0;
	}
	while (fscanf(fichier, "%c %c %d %d |\n", &couleur, &genre, &posX, &posY) != EOF){
		creerUnite(genre, &unite);
		switch (couleur){
			case ROUGE: placerAuMonde(unite, monde, posX, posY, couleur); break;
			case BLEU: placerAuMonde(unite, monde, posX, posY, couleur); break;
			case POTION: placerAuMonde(unite, monde, posX, posY, couleur); break;
		}
	}

	fclose(fichier);
	return 1;
}
