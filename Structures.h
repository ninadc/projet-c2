/* DE CASTRO, COMBE
 * TD2
 * Structures.h */

#ifndef __STRUCTURES__
#define __STRUCTURES__

#include <stdlib.h>

#define LONG 12
#define LARG 18
#define ROUGE 'R'
#define BLEU 'B'
#define SERF 's'
#define GUERRIER 'g'
#define POTION 'p'
#define MORT 'm'
#define VIE 'v'

typedef struct unite{
    int posX, posY;
    char couleur;
    char genre;
    struct unite *suiv;
}Unite, *UListe;

typedef struct monde{
    Unite *plateau[LONG][LARG];
    int tour;
    UListe rouge, bleu, potion;
	char nom1[255];
	char nom2[255];
}Monde;


/* Description: Allocation et adressage d'un Unite
 * Paramètres: un pointeur sur une UListe et un char 'type'
 * Renvoie 0 en cas d'erreurs, 1 sinon */
int creerUnite(char type, UListe *unite);

/* Description: initialisation du Monde
 * Paramètres: un pointeur vers un Monde 'monde'
 * Renvoie rien */
void initializerMonde(Monde *monde);

/* Description: génération nombre aléatoire compris entre 0 et nb
 * Paramètres: un int nb
 * Renvoie un int */
int randomInt(int nb);

/* Description: test d'ocupation d'une case
 * Paramètres: une UListe lst, et deux int 'posX' et 'posY'
 * Renvoie 1 si la case est occupée, 0 sinon */
int dejaOccupee(UListe lst, int posX, int posY);

/* Description: place un Unite dans un Monde
 * Paramètres: un pointeur sur un Unite, un pointeur sur un Monde, deux int 'posX' et 'posY' et un char 'couleur'
 * Renvoie 1 le Unite a bien été placé, 0 sinon */
int placerAuMonde(Unite *unite, Monde *monde, int posX, int posY, char couleur);

/* Description: test d'existence d'un Unite
 * Paramètres: une UListe lst, et un Unite 'unite'
 * Renvoie 1 si le unite existe, 0 sinon */
int uniteExist(UListe lst, Unite unite);

/* Description: déplace un Unite vers une position spécifiée dans un Monde
 * Paramètres: un pointeur sur un Unite, un pointeur sur un Monde, deux int 'destX' et 'destY'
 * Renvoie 0 en cas d'erreurs, 1 sinon */
int deplacerUnite(Unite *unite, Monde *monde, int destX, int destY);

/* Description: fonction auxiliaire qui supprime un Unite d'une UListe
 * Paramètres: un pointeur sur un Unite, un pointeur sur un Monde et un pointeur sur une UListe
 * Renvoie  0 en cas d'erreurs, 1 sinon */
int suppUniteListe(UListe *lst, Unite *unite, Monde *monde);

/* Description: enlève un Unite du monde
 * Paramètres: un pointeur sur un Unite, un pointeur sur un Monde
 * Renvoie  0 en cas d'erreurs, 1 sinon */
int enleverUnite(Unite *unite, Monde *monde);

/* Description: insertion d'un unite dans une liste après voir manger une potion
 * Paramètres: un pointeur sur un Monde et un char 'couleur'
 * Renvoie 0 en cas d'erreurs, 1 sinon*/
int insererUnite(UListe lst, Monde *monde, char couleur);

/* Description: attaque d'une unite sur une autre
 * Paramètres: un pointeur sur un Unite, un pointeur sur un Monde et deux int 'posX' et 'posY'
 * Renvoie  1 si l'unité attaquante gagne, 0 si elle perd, -1 en cas d'erreurs */
int attaquer(Unite *unite, Monde *monde, int posX, int posY);

/* Description: teste si une unite est voisine à une case
 * Paramètres: un pointeur sur un Unite et deux int 'posX' et 'posY'
 * Renvoie 1 si elle est voisine, 0 sinon */
int estVoisine(Unite unite, int destX, int destY);

/* Description: vide le monde
 * Paramètres: un pointeur sur un Monde
 * Renvoie rien */
void viderMonde(Monde *monde);


#endif
