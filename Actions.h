/* DE CASTRO, COMBE
 * TD2
 * Actions.h */

 #ifndef __ACTION__
 #define __ACTION__
#include "Graphic.h"

/* Description: gère les déplacements et les attaques
* Paramètres: un pointeur sur un Unite, un pointeur sur un Monde et deux int 'destX' et 'destY'
* Renvoie  1 si l'unité attaquante gagne, 0 si elle perd, -1 en cas d'erreurs */
int deplacerOuAttaquer(Unite *unite, Monde *monde, int destX, int destY);

/* Description: gère les coordonées saisies par un joueur
* Paramètres: un pointeur sur UListe 'tmp, un pointeur sur Monde 'monde' et un pointeur sur une MLV_Font
* Renvoie rien */
void gereCoordonnees(UListe tmp, Monde *monde, int x, int y, MLV_Font * font2);

/* Description: définit l'action en fonction de la zone de clic
* Paramètres: deux int x et y, et un pointeur sur MLV_Font
* Renvoie 1 s'il est dans la zone de jeu, 2 s'il choisit de ne rien faire, 3 s'il choisit de passer son tour, 0 en cas d'erreur */
int gereClic(UListe tmp, Monde *monde, int x, int y,  MLV_Font * font2);

/* Description: gère les actions d'un tour
* Paramètres: un char 'joueur', un pointeur sur un Monde et deux pointeurs sur MLV_Font
* Renvoie rien */
void gererDemiTour(char joueur, Monde *monde, MLV_Font * font, MLV_Font * font2);

/* Description: gère un tour complet de jeu
* Paramètres: un pointeur sur un Monde, deux pointeurs sur MLV_Font et un int 'nb'
* Renvoie rien */
void gererTour(Monde *monde, MLV_Font * font, MLV_Font * font2, int nb);


/* Description: génération d'une UListe aléatoire de taille nb
* Paramètres: une UListe lst, un pointeur sur Monde 'monde', un char 'couleur' et un int 'nb'
* Renvoie 0 en cas d'erreurs, 1 sinon */
int createListe(UListe lst, Monde *monde, char couleur, int nb);

/* Description: effectue une partie du jeu
* Paramètres: rien
* Renvoie rien */
void gererPartie(void);

#endif
