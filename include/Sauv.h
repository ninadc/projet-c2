/* DE CASTRO, COMBE
 * TD2
 * Sauv.h */

#ifndef __SAUV__
#define __SAUV__

#include <stdlib.h>
#include <stdio.h>
#include "../include/Structures.h"

/* Description: écrit une liste dans un fichier
* Paramètres: une UListe 'lst' et un pointeur vers un FILE 'fichier'
* Renvoie  rien */
void ecritListe(FILE *fichier, UListe lst);

/* Description: écrit une sauvegarde dans un fichier text
* Paramètres: trois UListe 'lstR', 'lstB' et 'lstP'
* Renvoie  rien */
void ecritFichier(UListe lstR, UListe lstB, UListe lstP);

/* Description: recup une sauvegarde et modifie le monde en conséquence
* Paramètres: un pointeur sur un Monde 'monde'
* Renvoie  rien */
int recupSauv(Monde *monde);

#endif
