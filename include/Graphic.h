/* DE CASTRO, COMBE
 * TD2
 * Graphic.h */

#ifndef __GRAPHIC__
#define __GRAPHIC__

#include "../include/Sauv.h"
#include <MLV/MLV_all.h>
#define CASE 45
#define INTERLIGNE 1

/* Description: création de la fenêtre de l'interface
 * Paramètres: rien
 * Renvoie rien */
void fenetre();

/* Description: affichage d'un plateau d'un Monde
 * Paramètres: un Monde 'monde'
 * Renvoie rien */
void affichePlateau(Monde monde);

/* Description: affichage des messages d'erreurs
 * Paramètres: une chaine de caractères 'chaine' et un pointeur sur une MLV_Font
 * Renvoie rien */
void afficheErreur(char *chaine, MLV_Font * font2);

/* Description: dessine le quadrillage du plateau
 * Paramètres: rien
 * Renvoie rien */
void dessineCadrillage();

/* Description: affichage des données des joueurs
 * Paramètres: un char 'couleur', deux chaînes de caractère 'nom1' et 'nom2', deux int 'posX' et 'posY', et un pointeur sur MLV_Font
 * Renvoie rien */
void afficheDonnees(char couleur, char *nom1, char *nom2, int posX, int posY, MLV_Font * font);

/* Description: affichage des options de jeu
 * Paramètres: un pointeur sur MLV_Font
 * Renvoie rien */
void dessineOptions(MLV_Font * font);

/* Description: dessine un serf à partir d'une image png
 * Paramètres: un char 'couleur' et deux int 'posX' et 'posY'
 * Renvoie rien */
void dessineSerf(char couleur, int posX, int posY);

/* Description: dessine un poison à partir d'une image png
 * Paramètres: deux int 'posX' et 'posY'
 * Renvoie rien */
void dessineMort(int posX, int posY);

/* Description: dessine une potion à partir d'une image png
 * Paramètres: deux int 'posX' et 'posY'
 * Renvoie rien */
void dessineVie(int posX, int posY);

/* Description: dessine un guerrier à partir d'une image png
 * Paramètres: un char 'couleur' et deux int 'posX' et 'posY'
 * Renvoie rien */
void dessineGuerrier(char couleur, int posX, int posY);

/* Description: affichage du gagnant du jeu
 * Paramètres: une chaîne de caractère 'chaine' et un pointeur sur une MLV_Font
 * Renvoie rien */
void afficheGagnant(char *chaine, MLV_Font * font3);

/* Description: affichage menu qui permet de saisir les noms des joueurs
 * Paramètres: un pointeur sur un monde et deux pointeurs sur une MLV_Font
 * Renvoie rien */
int accueil(Monde *monde, MLV_Font *font3, MLV_Font *font, MLV_Font *font2);

/* Description: affichage d'un UListe
 * Paramètres: un Monde 'monde'
 * Renvoie rien */
void afficheListe(UListe lst);

#endif
